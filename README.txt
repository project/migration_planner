Drush 9 command used to query a Drupal 7 site database and generate an Excel file that can be used to plan migrations.

# How to get started
---------------------

1. Initialize a new Drupal 8 site, and ensure the Drupal 7 database is registered in the settings.php file as "drupal_7".
2. Add the module to composer by running `composer require drupal/migration_planner`.
3. Enable the module using drush by running `drush en migration_planner`.

# How to use
-------------

1. Run the drush command `drush migration_planner:generate`.
2. Go the site's files directory, within the "migration_planner" directory to find the generated Excel file.

# Copying Conditional Formatting
---------------------------------

Google Sheet example with conditional formatting: https://docs.google.com/spreadsheets/d/1SxVNqEeNvRHgIRDuz9fUKLLY4Kog0SkeSOzJxQP4Jwk/edit?usp=sharing
1. Create a new Google Sheet and import your Excel file.
2. Go to the Google Sheet example above and copy one of the tabs to your Google Sheet.
3. Select all cells in the copied tab and select "Copy".
4. Go to each tab in your Google Sheet, select all cells, right click and select "Paste special > Paste conditional formatting only".
5. Delete the copied tab.
