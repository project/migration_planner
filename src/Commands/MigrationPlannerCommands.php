<?php

namespace Drupal\migration_planner\Commands;

use Drush\Commands\DrushCommands;
use Drupal\Core\Database\Database;
use Drupal\Core\File\FileSystem;
use Drush\Log\LogLevel;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Exception;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 */
class MigrationPlannerCommands extends DrushCommands {

  /**
   * Variable to store the Drupal 7 database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystem
   */
  protected $fileSystem;

  /**
   * Constructs a new DrushCommands object.
   *
   * @param \Drupal\Core\File\FileSystem $file_system
   *   The file system service.
   */
  public function __construct(FileSystem $file_system) {
    parent::__construct();
    $this->fileSystem = $file_system;
  }

  /**
   * Generates an Excel file from a Drupal 7 database for migration planning.
   *
   * @command migration_planner:generate
   * @usage migration_planner:generate
   *   Generate an Excel file for planning a Drupal 7 migration.
   */
  public function generate() {
    // Core entity type label field.
    $core_entity_type_label = [
      'node' => 'title',
      'comment' => 'subject',
      'file' => 'filename',
      'taxonomy_term' => 'name',
    ];
    // Set the connection to the Drupal 7 database host.
    $this->connection = Database::getConnection('default', 'drupal_7');
    // Query the Drupal 7 database for field instances information.
    $result = $this->getFieldInstances();
    if (!$result['result']) {
      return;
    }
    // Loop through result rows and prepare the row data.
    $result_rows = [];
    foreach ($result['result'] as $row) {
      $result_rows[$row->entity_type][$row->bundle][$row->field_name] = [
        'entity_type' => $row->entity_type,
        'bundle' => $row->bundle,
        'field_name' => $row->field_name,
        'field_type' => $row->type,
      ];
    }
    // Add entity label field rows.
    foreach ($core_entity_type_label as $entity_type => $label) {
      if (isset($result_rows[$entity_type])) {
        foreach ($result_rows[$entity_type] as $bundle => $fields) {
          $label_field = [
            'entity_type' => $entity_type,
            'bundle' => $bundle,
            'field_name' => $label,
            'field_type' => 'text',
          ];
          array_unshift($fields, $label_field);
          $result_rows[$entity_type][$bundle] = $fields;
        }
      }
    }
    // Flatten the multidimensional array to field level.
    $sheet_rows = [];
    foreach ($result_rows as $entity_type => $bundle) {
      foreach ($bundle as $field_name => $data) {
        $sheet_rows = array_merge($sheet_rows, array_values($data));
      }
    }
    // Loop through field data and prepare the spreadsheet row data.
    $sheets = [];
    foreach ($sheet_rows as $row) {
      $sheets[$row['entity_type']][] = [
        'D7 Bundle' => $row['bundle'],
        'D8 Entity Type' => '',
        'D8 Bundle' => '',
        'Source' => '',
        'Migration ID' => '',
        'D7 Field Name' => $row['field_name'],
        'D7 Field Type' => $row['field_type'],
        'D8 Field Name' => '',
        'Complexity' => '',
        'Status' => '',
        'Ticket' => '',
        'Notes' => '',
      ];
    }
    try {
      $this->generateExcelFile($sheets);
    } catch (Exception $e) {
      // Log error.
      $this->logger()
        ->log(LogLevel::ERROR, dt('There was a problem generating the Excel file.'));
    } catch (\PhpOffice\PhpSpreadsheet\Exception $e) {
      // Log creation of the Excel file.
      $this->logger()
        ->log(LogLevel::ERROR, dt('There was a problem generating the Excel file.'));
    }
  }

  /**
   * Helper function to retrieve paragraphs bundles.
   */
  private function getFieldInstances() {
    // Get a list of field instances.
    $query = $this->connection->select('field_config_instance', 'fci');
    $query->fields('fci', [
      'field_name',
      'entity_type',
      'bundle',
      'data',
    ]);
    $query->leftJoin(
      'field_config',
      'fc',
      "fci.field_name = fc.field_name"
    );
    $query->fields('fc', ['type']);
    $query->orderBy('entity_type', 'ASC');
    $query->orderBy('bundle', 'ASC');
    $query->orderBy('field_name', 'ASC');
    $result = $query->distinct()->execute();
    // Get the number of results.
    $result_count = $query->countQuery()->execute()->fetchField();
    return [
      'count' => $result_count,
      'result' => $result,
    ];
  }

  /**
   * Helper function to generate an Excel file from report data.
   *
   * @param $sheets
   *   Data for each sheet of the Excel file.
   *
   * @throws \PhpOffice\PhpSpreadsheet\Exception
   * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
   */
  private function generateExcelFile($sheets) {
    // Initialize a new Spreadsheet object.
    $spreadsheet = new Spreadsheet();
    // Get current date.
    $date = date('Y-m-d');
    // Get the real path of the files that will be generated.
    $files_directory = $this->fileSystem->realpath('public://');
    $file_name = $files_directory . '/migration_planner/' . $date . '.xlsx';
    // Initialize the report counter.
    $sheet_index = 0;
    foreach ($sheets as $sheet => $data) {
      // Create report.
      if ($sheet_index > 0) {
        $spreadsheet->createSheet();
      }
      // Set the active sheet to the current report index.
      $spreadsheet->setActiveSheetIndex($sheet_index);
      // Set the worksheet title.
      $sheet_type = str_replace(' ', '_', $sheet);
      $spreadsheet->getActiveSheet()->setTitle($sheet_type);
      $headers = array_keys($data[0]);
      array_unshift($data, $headers);
      // Write report data and set auto filter for the data range.
      $spreadsheet->getActiveSheet()
        ->fromArray($data, 'A1')
        ->setAutoFilter(
          $spreadsheet->getActiveSheet()
            ->calculateWorksheetDimension()
        );
      // Get the highest column with data.
      $highest_column = $spreadsheet->getActiveSheet()
        ->getHighestDataColumn();
      // Autosize the column widths.
      for ($col = 'A'; $col < $highest_column; $col++) {
        $spreadsheet->getActiveSheet()
          ->getColumnDimension($col)
          ->setAutoSize(TRUE);
      }
      // Freeze first row.
      $spreadsheet->getActiveSheet()
        ->freezePane('A2');
      // Bold first row.
      $spreadsheet->getActiveSheet()
        ->getStyle('A1:' . $highest_column . '1')
        ->getFont()
        ->setBold(TRUE);
      // Log the report worksheet.
      $this->logger()
        ->log(LogLevel::SUCCESS, dt('The following sheet was added: ' . $sheet_type));
      // Increment the report index.
      $sheet_index++;
    }
    // Set the active sheet to the first.
    $spreadsheet->setActiveSheetIndex(0);
    // Create the Excel file.
    $xlsx_writer = new Xlsx($spreadsheet);
    $xlsx_writer->save($file_name);
    // Log creation of the Excel file.
    $this->logger()
      ->log(LogLevel::SUCCESS, dt('The following report file was generated: ' . $file_name));
  }

}
